// Initialize Environment Variables
require('dotenv').config({ path: './.env' });

// Initialize Application
const port = parseInt(process.env.PORT) || 80;
const size = parseInt(process.env.ARRAY_SIZE) || 10000;

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const bodyParser = require('body-parser');

app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function getRandomNumbers() {
	// Create an array from 1 to max size
	var arr = [ ...Array(size+1).keys() ].slice(1);

	// Shuffle the array
	return shuffle(arr);
}

/**
 * Shuffles an array using the Fisher-Yates algorithm.
 */
function shuffle(array) {
	let currentIndex = array.length;
	let randomIndex;

	// While there are still elements to shuffle
	while(currentIndex != 0) {
		// Pick an element
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;

		// And swap it with the current element
		[ array[currentIndex], array[randomIndex] ] = [ array[randomIndex], array[currentIndex] ];
	}

	return array;
}

app.get('/', (req, res) => {
	res.render('app', { data: size });
});

app.post('/grid', (req, res) => {
	const arr = getRandomNumbers();

	res.send(arr);
});

server.listen(port, () => console.log('[ SERVER ]: Started'));