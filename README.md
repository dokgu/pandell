# Coding Challenge

> Write a program that generates a list of 10,000 numbers in random order each time it is run. Each number in the list must be unique and be between 1 and 10,000 (inclusive).

## Instructions

1. `git clone git@gitlab.com:dokgu/pandell.git` - clone repository
2. `cd pandell` - go to the project directory
3. `npm install` - install project dependencies
4. `cp .env-example .env` - make a copy of `.env-example` and rename the copy to `.env`
5. `nano .env` - adjust environment variables as needed
6. `npx nodemon app.js` - start the server